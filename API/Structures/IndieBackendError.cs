﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace IndieBackendAdmin.API.Structures
{
	[JsonConverter(typeof(IndieBackendErrorConverter))]
	public class IndieBackendError : Exception
	{
		public string Status;
		public new string Message;
		public IErrorDetails Details;
	}

	public class IndieBackendErrorConverter : JsonConverter
	{

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
			{
				return string.Empty;
			}
			else if (reader.TokenType == JsonToken.String)
			{
				return serializer.Deserialize(reader, objectType);
			}
			else
			{
				JObject obj = JObject.Load(reader);

				IndieBackendError err = new IndieBackendError();

				if (obj["status"] != null)
					err.Status = obj["status"].ToString();
				if (obj["message"] != null)
					err.Message = obj["message"].ToString();

				if (obj["details"] == null)
				{
					err.Details = null;
					return err;
				}

				if (err.Message == "Invalid payload")
				{
					JArray arr = (JArray)obj["details"];

					err.Details = new InvalidPayload()
					{
						Field = arr[0]["field"].ToString(),
						Error = arr[0]["error"].ToString(),
					};

				}

				return err;
			}
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}
		public override bool CanConvert(Type objectType)
		{
			throw new NotImplementedException();
		}
	}

	public interface IErrorDetails { }

	public class InvalidPayload : IErrorDetails {
		public string Field;
		public string Error;
	}

}
