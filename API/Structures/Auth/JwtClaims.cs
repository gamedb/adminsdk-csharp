﻿namespace IndieBackendAdmin.API.Structures.Auth
{
	/// <summary>
	/// JsonWebTokens claims
	/// </summary>
	public struct JwtClaims
	{
		/// <summary>
		/// Token's issuer (IndieBackend)
		/// </summary>
		public string Iss;
		/// <summary>
		/// Token's subject (player or profile Id)
		/// </summary>
		public string Sub;
		/// <summary>
		/// Token's audience (application Id)
		/// </summary>
		public string Aud;
		/// <summary>
		/// Token's type
		/// </summary>
		public string Type;
		/// <summary>
		/// Token creation date in seconds
		/// </summary>
		public long Iat;
		/// <summary>
		/// Token expiration date in seconds
		/// </summary>
		public long Exp;
		/// <summary>
		/// Token's unique Id
		/// </summary>
		public string Jti;
	}

}
