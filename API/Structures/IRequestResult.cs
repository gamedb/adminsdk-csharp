﻿namespace IndieBackendAdmin.API.Structures
{
	public abstract class IRequestResult
	{

		public bool Success;
		public IndieBackendError Error;

	}
}
