﻿namespace IndieBackendAdmin.API
{
	internal class Constants
	{

		public const string GROUPS_URL = "http://localhost:8015";
		public const string AUTH_URL = "http://localhost:8008";
		public const string LEADERBOARDS_URL = "http://localhost:8010";
		public const string STATISTICS_URL = "http://localhost:8017";
		public const string CURRENCIES_URL = "http://localhost:8029";

	}
}
