﻿using IndieBackendAdmin.API.Services;
using System.Net.Http;

namespace IndieBackendAdmin.API
{

	public class IndieBackendAPI
	{

		public Auth Auth { get; private set; }
		public Groups Groups { get; private set; }
		public Leaderboards Leaderboards { get; private set; }
		public Statistics Statistics { get; private set; }
		public Currencies Currencies { get; private set; }

		private readonly string apiKey;
		private readonly string apiSecret;

		public IndieBackendAPI(string apiKey, string apiSecret)
		{
			this.apiKey = apiKey;
			this.apiSecret = apiSecret;

			//Services initialization
			Auth = new Auth(apiKey, apiSecret);
			Groups = new Groups(apiKey, apiSecret);
			Leaderboards = new Leaderboards(apiKey, apiSecret);
			Statistics = new Statistics(apiKey, apiSecret);
			Currencies = new Currencies(apiKey, apiSecret);
		}

	}
}
