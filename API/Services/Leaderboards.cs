﻿using IndieBackendAdmin.API.Structures;
using IndieBackendAdmin.API.Utils;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace IndieBackendAdmin.API.Services
{
	/// <summary>
	/// Class representing the leaderboards service
	/// </summary>
	public class Leaderboards : IService
	{

		public Leaderboards(string apiKey, string apiSecret)
			: base(apiKey, apiSecret, Constants.LEADERBOARDS_URL) { }


		/// <summary>
		/// Set a record in leaderboard
		/// </summary>
		/// <param name="leaderboardName">Leaderboard to updated</param>
		/// <param name="id">Record id</param>
		/// <param name="score">Score associated with id</param>
		/// <returns></returns>
		public Task<SetLbRecordResult> Set(string leaderboardName, string id, double score)
		{
			return Post<SetLbRecordResult>($"/{leaderboardName}", new SetLbRecordRequest()
			{
				id = id,
				score = score
			}, AuthMethods.API_SECRET) ;
		}
		/// <summary>
		/// Get a leaderboard's record
		/// </summary>
		/// <param name="leaderboardName">Leaderboard name</param>
		/// <param name="id">Record id to fetch</param>
		/// <returns></returns>
		public Task<GetLbRecordResult> Get(string leaderboardName, string id)
		{
			return Get<GetLbRecordResult>($"/{leaderboardName}/{id}", AuthMethods.API_KEY);
		}
		/// <summary>
		/// Count number of records in leaderboard
		/// </summary>
		/// <param name="leaderboardName">Leaderboard name</param>
		/// <returns></returns>
		public Task<LbRecordCountResult> Count(string leaderboardName)
		{
			return Get<LbRecordCountResult>($"/{leaderboardName}/count", AuthMethods.API_SECRET);
		}
		/// <summary>
		/// Get records between two boundaries
		/// </summary>
		/// <param name="leaderboardName">Leaderboard name</param>
		/// <param name="from">From boundary</param>
		/// <param name="to">To boundary</param>
		/// <param name="mode">Range mode (eitheir ranks or score)</param>
		/// <returns></returns>
		public Task<LbRangeResult> Range(string leaderboardName, int from, int to, string mode = "rank")
		{
			return Get<LbRangeResult>($"/{leaderboardName}/{from}/{to}?mode={mode}", AuthMethods.API_KEY);
		}
		/// <summary>
		/// Delete a record from a leaderboard
		/// </summary>
		/// <param name="leaderboardName">Leaderboard name</param>
		/// <param name="id">Record id to delete</param>
		/// <returns></returns>
		public Task<LbDeleteRecordResult> Delete(string leaderboardName, string id)
		{
			return Delete<LbDeleteRecordResult>($"/{leaderboardName}/{id}", AuthMethods.API_SECRET);
		}
		/// <summary>
		/// Clear leaderboard of all records
		/// </summary>
		/// <param name="leaderboardName">Leaderboard name</param>
		/// <returns></returns>
		public Task<LbClearResult> Clear(string leaderboardName)
		{
			return Delete<LbClearResult>($"/{leaderboardName}", AuthMethods.API_SECRET);
		}
		/// <summary>
		/// Increment a leaderboard record
		/// </summary>
		/// <param name="leaderboardName">Leaderboard name</param>
		/// <param name="id">Record id to increment</param>
		/// <param name="amount">Amount to increment by</param>
		/// <returns></returns>
		public Task<IncrementLbRecordResult> Increment(string leaderboardName, string id, double amount)
		{
			return Post<IncrementLbRecordResult>($"/{leaderboardName}/increment", new IncrementLbRecordRequest()
			{
				id = id,
				amount = amount
			}, AuthMethods.API_SECRET);
		}
	}

	internal class SetLbRecordRequest
	{
		public string id;
		public double score;
	}
	/// <summary>
	/// Set record result
	/// </summary>
	public class SetLbRecordResult : IRequestResult
	{
		/// <summary>
		/// Amount of record's added
		/// </summary>
		public int Added;
	}
	/// <summary>
	/// Get record result
	/// </summary>
	public class GetLbRecordResult : IRequestResult
	{
		/// <summary>
		/// Record's score
		/// </summary>
		public double? Score;
		/// <summary>
		/// Record's rank
		/// </summary>
		public int? Rank;
		/// <summary>
		/// Does record exist ?
		/// </summary>
		public bool Exist;
	}
	/// <summary>
	/// Count record's in leaderboard in results
	/// </summary>
	public class LbRecordCountResult : IRequestResult
	{
		/// <summary>
		/// Number of records in leaderboard
		/// </summary>
		public int Count;
	}
	/// <summary>
	/// Records fetched in range
	/// </summary>
	public class LbRangeResult : IRequestResult
	{
		/// <summary>
		/// Fetched records
		/// </summary>
		public Dictionary<string, double> Result;
	}
	/// <summary>
	/// Leaderboard clear result
	/// </summary>
	public class LbClearResult : IRequestResult
	{
		/// <summary>
		/// Has leaderboard been cleared
		/// </summary>
		public bool Cleared;
	}
	/// <summary>
	/// Record's delete result
	/// </summary>
	public class LbDeleteRecordResult : IRequestResult
	{
		/// <summary>
		/// Amount of records
		/// </summary>
		public int Deleted;
	}
	public class IncrementLbRecordRequest
	{
		public string id;
		public double amount;
	}
	/// <summary>
	/// Increment leaderboard result
	/// </summary>
	public class IncrementLbRecordResult : IRequestResult
	{
		/// <summary>
		/// Id of the incremented record
		/// </summary>
		public string Id;
		/// <summary>
		/// INcremented score
		/// </summary>
		public double Score;
	}
}
