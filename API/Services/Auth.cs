﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IndieBackendAdmin.API.Structures;
using IndieBackendAdmin.API.Structures.Auth;
using IndieBackendAdmin.API.Utils;

namespace IndieBackendAdmin.API.Services
{
	/// <summary>
	/// Class representing the authentication service
	/// </summary>
	public class Auth : IService
	{

		public Auth(string apiKey, string apiSecret)
			: base(apiKey, apiSecret, Constants.AUTH_URL) { }

		/// <summary>
		/// Check if a token is a valid IndieBackend token
		/// </summary>
		/// <param name="token">Token to check</param>
		/// <returns></returns>
		public Task<VerifyTokenResult> VerifyToken(string token)
		{
			return Post<VerifyTokenResult>("/verify", new VerifyTokenRequest { token = token }, AuthMethods.NONE);
		}

	}

	public struct VerifyTokenRequest
	{
		public string token;
	}

	/// <summary>
	/// 
	/// </summary>
	public class VerifyTokenResult : IRequestResult
	{
		/// <summary>
		/// Claims of this token
		/// </summary>
		public JwtClaims Claims;
		/// <summary>
		/// Token type (either profile or auth)
		/// </summary>
		public string Type;
	}
}
