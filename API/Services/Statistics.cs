﻿using IndieBackendAdmin.API.Structures;
using IndieBackendAdmin.API.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndieBackendAdmin.API.Services
{
	public class Statistics : IService
	{

		public Statistics(string apiKey, string apiSecret)
			: base(apiKey, apiSecret, Constants.STATISTICS_URL) { }

		public Task<SetPlayerStatsResult> SetPlayer(string profileId, Dictionary<string, double> stats)
		{
			if (string.IsNullOrEmpty(profileId))
				throw new ArgumentException("profileId cannot be null or empty");

			return Post<SetPlayerStatsResult>("/players/" + profileId, new UpdatePlayerStatsRequest(stats), AuthMethods.API_SECRET);
		}
		public Task<SetPlayerStatsResult> SetPlayer(string profileId, string statsName, double value)
		{
			if (string.IsNullOrEmpty(profileId))
				throw new ArgumentException("profileId cannot be null or empty");

			return Post<SetPlayerStatsResult>("/players/" + profileId, new UpdatePlayerStatsRequest(statsName, value), AuthMethods.API_SECRET);
		}

		public Task<IncrementPlayerStatsResult> IncrementPlayer(string profileId, Dictionary<string, double> stats)
		{
			return Patch<IncrementPlayerStatsResult>("/players/" + profileId, new UpdatePlayerStatsRequest(stats), AuthMethods.API_SECRET);
		}
		public Task<IncrementPlayerStatsResult> IncrementPlayer(string profileId, string statsName, double value)
		{
			return Patch<IncrementPlayerStatsResult>("/players/" + profileId, new UpdatePlayerStatsRequest(statsName, value), AuthMethods.API_SECRET);
		}

		public Task<GetPlayerStatsResult> GetPlayer(string profileId)
		{
			return Get<GetPlayerStatsResult>("/players/" + profileId, AuthMethods.API_SECRET);
		}

		public Task<DeletePlayerStatsResult> DeletePlayer(string profileId, string[] fields)
		{
			return Delete<DeletePlayerStatsResult>("/players/" + profileId, new DeletePlayerStatsRequest(fields), AuthMethods.API_SECRET);
		}
		public Task<DeletePlayerStatsResult> DeletePlayer(string profileId, List<string> fields)
		{
			return DeletePlayer(profileId, fields.ToArray());
		}
	}

	public class UpdatePlayerStatsRequest
	{
		public Dictionary<string, double> stats;

		public UpdatePlayerStatsRequest(Dictionary<string, double> stats)
		{
			this.stats = stats;
		}

		public UpdatePlayerStatsRequest(string name, double value)
		{
			stats = new Dictionary<string, double>()
			{
				{ name, value }
			};
		}

	}

	public class SetPlayerStatsResult : IRequestResult
	{
		public bool Updated;
		public Dictionary<string, double> Stats;
	}

	public class IncrementPlayerStatsResult : IRequestResult
	{
		public bool Incremented;
		public Dictionary<string, double> Stats;
	}

	public class GetPlayerStatsResult : IRequestResult
	{
		public Dictionary<string, double> Stats;
	}

	public class DeletePlayerStatsRequest
	{
		public string[] fields;

		public DeletePlayerStatsRequest(string[] fields)
		{
			this.fields = fields;
		}
	}
	public class DeletePlayerStatsResult : IRequestResult
	{
		public bool Deleted;
	}

}
