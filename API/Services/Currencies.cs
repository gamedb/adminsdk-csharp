﻿using IndieBackendAdmin.API.Structures;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IndieBackendAdmin.API.Services
{
	/// <summary>
	/// Class representing the currencies service
	/// </summary>
	public class Currencies : IService
	{

		public Currencies(string apiKey, string apiSecret)
			: base(apiKey, apiSecret, Constants.CURRENCIES_URL) { }

		/// <summary>
		/// Get currencies and their corresponding amount for a specific profile
		/// </summary>
		/// <param name="profileId">Profile Id to lookup for</param>
		/// <returns></returns>
		public Task<ProfileCurrenciesResult> Get(string profileId)
		{
			return Get<ProfileCurrenciesResult>("/players/" + profileId, Utils.AuthMethods.API_KEY);
		}

		/// <summary>
		/// Set profile's currencies to a specific values
		/// </summary>
		/// <param name="profileId">Profile Id to update</param>
		/// <param name="currencies">Dictionary reprensenting the currencies names and their corresponding values</param>
		/// <returns></returns>
		public Task<ProfileCurrenciesResult> Set(string profileId, Dictionary<string, double> currencies)
		{
			return Post<ProfileCurrenciesResult>("/players/" + profileId, currencies, Utils.AuthMethods.API_SECRET);
		}
		/// <summary>
		/// Set a profile currency to a specific value
		/// </summary>
		/// <param name="profileId">Profile Id to update</param>
		/// <param name="currencyName">Currency name to set</param>
		/// <param name="amount">Value of the currency to set</param>
		/// <returns></returns>
		public Task<ProfileCurrenciesResult> Set(string profileId, string currencyName, double amount)
		{
			return Set(profileId, new Dictionary<string, double>() { { currencyName, amount } });
		}

		/// <summary>
		/// Increment profiles currencies by a specific amount
		/// </summary>
		/// <param name="profileId">Profile Id to update</param>
		/// <param name="currencies">Dictionary reprensenting the currencies names and their corresponding values</param>
		/// <returns></returns>
		public Task<ProfileCurrenciesResult> Increment(string profileId, Dictionary<string, double> currencies)
		{
			return Patch<ProfileCurrenciesResult>("/players/" + profileId, currencies, Utils.AuthMethods.API_SECRET);
		}
		/// <summary>
		/// Increment a profile specific currencies 
		/// </summary>
		/// <param name="profileId">Profile Id to update</param>
		/// <param name="currencyName">Currency name to increment</param>
		/// <param name="amount">Amount to increment by</param>
		/// <returns></returns>
		public Task<ProfileCurrenciesResult> Increment(string profileId, string currencyName, double amount)
		{
			return Increment(profileId, new Dictionary<string, double>() { { currencyName, amount } });
		}

	}

	/// <summary>
	/// Snapshot of the current state of this account
	/// </summary>
	public class ProfileCurrenciesResult : IRequestResult
	{
		/// <summary>
		/// Profile's currencies
		/// </summary>
		public Dictionary<string, double> Account;
		/// <summary>
		/// Has the reuquest updated the profile
		/// </summary>
		public bool Updated;
	}

}
