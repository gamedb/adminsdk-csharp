﻿using IndieBackendAdmin.API.Structures;
using IndieBackendAdmin.API.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IndieBackendAdmin.API.Services
{

	/// <summary>
	/// Class representing the groups service
	/// </summary>
	public class Groups : IService
	{

		public Groups(string apiKey, string apiSecret)
			: base(apiKey, apiSecret, Constants.GROUPS_URL) { }

		/// <summary>
		/// Create a new Group based on the provided settings
		/// </summary>
		/// <param name="rq">Group creation settings</param>
		/// <returns>Group data after it's creation</returns>
		public Task<GroupDataResult> Create(CreateGroupRequest rq)
		{
			return Post<GroupDataResult>("/groups", rq, AuthMethods.API_SECRET);
		}

		/// <summary>
		/// Get current group data
		/// </summary>
		/// <param name="id">Id of the group to fetch</param>
		/// <returns>Current group data</returns>
		public Task<GroupDataResult> Get(string id)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");

			return Get<GroupDataResult>("/groups/" + id, AuthMethods.API_SECRET);
		}

		/// <summary>
		/// Delete a group
		/// </summary>
		/// <param name="id">Id of the group to delete</param>
		/// <returns>Groupo deletion result</returns>
		public Task<DeleteGroupResult> Delete(string id)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");

			return Delete<DeleteGroupResult>("/groups/" + id, AuthMethods.API_SECRET);
		}

		/// <summary>
		/// Set group data
		/// </summary>
		/// <param name="id">Group id to update</param>
		/// <param name="data">Group's data</param>
		/// <returns></returns>
		public Task<SetGroupDataResult> SetData(string id, Dictionary<string, object> data)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");

			return Post<SetGroupDataResult>("/groups/" + id, data, AuthMethods.API_SECRET);
		}

		/// <summary>
		/// Add player to group
		/// </summary>
		/// <param name="id">Group id</param>
		/// <param name="playerId">profile id</param>
		/// <returns></returns>
		public Task<JoinGroupResult> Join(string id, string profileId)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");

			return Get<JoinGroupResult>($"/groups/{id}/join?userId={playerId}", AuthMethods.API_SECRET);
		}
		/// <summary>
		/// Remove player from group
		/// </summary>
		/// <param name="id">Group id</param>
		/// <param name="playerId">profile id</param>
		/// <returns></returns>
		public Task<LeaveGroupResult> Leave(string id, string profileId)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");

			return Get<LeaveGroupResult>($"/groups/{id}/leave?userId={playerId}", AuthMethods.API_SECRET);
		}
		
		/// <summary>
		/// Set group leader
		/// </summary>
		/// <param name="id">Group id</param>
		/// <param name="leaderId">Leader's profile id</param>
		/// <returns></returns>
		public Task<SetLeaderResult> SetLeader(string id, string leaderId)
		{
			if (string.IsNullOrEmpty(id))
				throw new ArgumentException("id cannot be null or empty");

			return Post<SetLeaderResult>($"/groups/{id}/leader?newLeader={leaderId}", new object(), AuthMethods.API_SECRET);
		}

	}

	/// <summary>
	/// Create group settings
	/// </summary>
	public struct CreateGroupRequest
	{
		/// <summary>
		/// Group's leader
		/// </summary>
		[JsonProperty("leader")] public string Leader;
		/// <summary>
		/// Will data be available when fetching group without using API Secret
		/// </summary>
		[JsonProperty("isDataPublic")] public bool IsDataPublic;
		/// <summary>
		/// Group's data
		/// </summary>
		[JsonProperty("data")] public Dictionary<string, object> Data;
		/// <summary>
		/// Group's members
		/// </summary>
		[JsonProperty("members")] public string[] Members;
	}

	/// <summary>
	/// Snapshot of current group data
	/// </summary>
	public class GroupDataResult : IRequestResult
	{
		/// <summary>
		/// Group's id
		/// </summary>
		public string Id;
		/// <summary>
		/// Group's leader
		/// </summary>
		public string Leader;
		/// <summary>
		/// Group's members
		/// </summary>
		public List<string> Members;
		/// <summary>
		/// Are data available when fetching group without using API Secret
		/// </summary>
		public bool IsDataPublic;
		/// <summary>
		/// Group's data
		/// </summary>
		public Dictionary<string, object> Data;
	}
	
	/// <summary>
	/// Group deletion result
	/// </summary>
	public class DeleteGroupResult : IRequestResult
	{
		/// <summary>
		/// Is group deleted ?
		/// </summary>
		public bool Deleted;
		/// <summary>
		/// Group id
		/// </summary>
		public string Id;
	}
	/// <summary>
	/// Set group data result
	/// </summary>
	public class SetGroupDataResult : IRequestResult
	{
		/// <summary>
		/// Is group data updated
		/// </summary>
		public bool Updated;
	}
	/// <summary>
	/// Join group result
	/// </summary>
	public class JoinGroupResult : IRequestResult
	{
		/// <summary>
		/// Has player joined group
		/// </summary>
		public bool Joined;
	}
	/// <summary>
	/// Leave group result
	/// </summary>
	public class LeaveGroupResult : IRequestResult
	{
		/// <summary>
		/// Has player left the group
		/// </summary>
		public bool Left;
	}
	/// <summary>
	/// Set leader result
	/// </summary>
	public class SetLeaderResult : IRequestResult
	{
		/// <summary>
		/// Group id
		/// </summary>
		public string Id;
		/// <summary>
		/// Has leader been set ?
		/// </summary>
		public bool Updated;
	}
	public struct SetLeaderRequest
	{
		public string Name;
	}
}
