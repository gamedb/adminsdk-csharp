﻿using IndieBackendAdmin.API.Structures;
using IndieBackendAdmin.API.Utils;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace IndieBackendAdmin.API.Services
{
	public abstract class IService
	{
		protected readonly string baseUrl;

		private readonly string apiKey;
		private readonly string apiSecret;

		public IService(string apiKey, string apiSecret, string baseUrl)
		{
			this.baseUrl = baseUrl;

			this.apiKey = apiKey;
			this.apiSecret = apiSecret;
		}

		protected Task<T> Get<T>(string url, AuthMethods method) where T : IRequestResult
		{
			return HttpUtils.Get<T>(BuildUrl(url, method));
		}
		protected Task<T> Post<T>(string url, object body, AuthMethods method) where T : IRequestResult
		{
			return HttpUtils.Post<T>(BuildUrl(url, method), body);
		}
		protected Task<T> Delete<T>(string url, AuthMethods method) where T : IRequestResult
		{
			return HttpUtils.Delete<T>(BuildUrl(url, method));
		}

		protected Task<T> Delete<T>(string url, object body, AuthMethods method) where T : IRequestResult
		{
			return HttpUtils.Delete<T>(BuildUrl(url, method), body);
		}

		protected Task<T> Patch<T>(string url,  object body, AuthMethods method) where T : IRequestResult
		{
			return HttpUtils.Patch<T>(BuildUrl(url, method), body);
		}

		private string BuildUrl(string endpoint, AuthMethods method)
		{
			string auth;
			switch (method)
			{
				case AuthMethods.API_KEY:
					auth = $"apiKey={apiKey}";
					break;
				case AuthMethods.API_SECRET:
					auth = $"apiSecret={apiSecret}";
					break;
				case AuthMethods.BOTH:
					auth = $"apiKey={apiKey}&apiSecret={apiSecret}";
					break;
				case AuthMethods.NONE:
					auth = "";
					break;
				default:
					auth = "";
					break;
			}
			//System.Console.WriteLine($"{baseUrl}{endpoint}{(endpoint.Contains("?") ? "&" + auth : "?" + auth)}");
			return $"{baseUrl}{endpoint}{(endpoint.Contains("?") ? "&" + auth : "?" + auth)}";
		}

	}
}
