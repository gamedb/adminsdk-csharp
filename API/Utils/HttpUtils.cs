﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using IndieBackendAdmin.API.Structures;
using Newtonsoft.Json;

namespace IndieBackendAdmin.API.Utils
{
	public class HttpUtils
	{
		private static readonly HttpClient client = new HttpClient();

		public static Task<T> Get<T>(string url) where T : IRequestResult
		{
			return Request<T>(HttpMethod.Get, url);
		}

		public static Task<T> Post<T>(string url, object body) where T : IRequestResult
		{
			return Request<T>(HttpMethod.Post, url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));
		}

		public static Task<T> Delete<T>(string url) where T : IRequestResult
		{
			return Request<T>(HttpMethod.Delete, url);
		}

		public static Task<T> Delete<T>(string url, object body) where T : IRequestResult
		{
			return Request<T>(HttpMethod.Delete, url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));
		}

		public static Task<T> Patch<T>(string url, object body) where T : IRequestResult
		{
			return Request<T>(HttpMethod.Patch, url, new StringContent(JsonConvert.SerializeObject(body), Encoding.UTF8, "application/json"));
		}

		public static async Task<T> Request<T>(HttpMethod method, string url, HttpContent content = null) where T : IRequestResult
		{
			using var request = new HttpRequestMessage(method, url);
			if (content != null)
				request.Content = content;
			//TODO: Use cancellation token ?
			using var response = await client.SendAsync(request);
			var res = DeserializeJsonFromStream<T>(await response.Content.ReadAsStreamAsync());

			if (!res.Success)
				throw res.Error;

			return res;
		}

		//Method from: https://johnthiriet.com/efficient-api-calls/
		private static T DeserializeJsonFromStream<T>(Stream stream)
		{
			if (stream == null || !stream.CanRead)
				return default(T);

			using var sr = new StreamReader(stream);
			using var jtr = new JsonTextReader(sr);
			var js = new JsonSerializer();
			return js.Deserialize<T>(jtr);
		}

	}
}
