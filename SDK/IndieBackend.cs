﻿using IndieBackendAdmin.API;
using IndieBackendAdmin.SDK.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace IndieBackendAdmin.SDK
{
	public class IndieBackend
	{

		public Groups Groups { get; private set; }
		public Leaderboards Leaderboards { get; private set; }
		public Statistics Statistics { get; private set; }
		public Auth Auth { get; private set; }
		public Currencies Currencies { get; private set; }

		private IndieBackendAPI api;

		public IndieBackend(string apiKey, string apiSecret)
		{
			api = new IndieBackendAPI(apiKey, apiSecret);

			Groups = new Groups(api.Groups);
			Leaderboards = new Leaderboards(api.Leaderboards);
			Statistics = new Statistics(api.Statistics);
			Auth = new Auth(api.Auth);
			Currencies = new Currencies(api.Currencies);
		}

	}
}
