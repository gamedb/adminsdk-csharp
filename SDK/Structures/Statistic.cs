﻿using IndieBackendAdmin.API.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Structures
{
	public class Statistic
	{

		public string Name { get; private set; }

		private Statistics api;

		public Statistic(string name, Statistics api)
		{
			Name = name;
			this.api = api;
		}

		public async Task<Dictionary<string, double>> Set(string profileId, double value)
		{
			var res = await api.SetPlayer(profileId, Name, value);
			return res.Stats;
		}

		public async Task<Dictionary<string, double>> Increment(string profileId, double amount)
		{
			var res = await api.IncrementPlayer(profileId, Name, amount);
			return res.Stats;
		}

		public async Task<Dictionary<string, double>> Get(string profileId)
		{
			var res = await api.GetPlayer(profileId);
			return res.Stats;
		}

		public async Task<bool> Delete(string profileId)
		{
			var res = await api.DeletePlayer(profileId, new string[] { Name });
			return res.Deleted;
		}
	}
}
