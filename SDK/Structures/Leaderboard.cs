﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Structures
{
	public class Leaderboard
	{

		public string Id { get; private set; }

		private API.Services.Leaderboards api;

		public Leaderboard(string id, API.Services.Leaderboards api)
		{
			Id = id;
			this.api = api;
		}

		public async Task<int> Set(string recordId, double score)
		{
			var res = await api.Set(Id, recordId, score);
			return res.Added;
		}
		public async Task<LeaderboardRecord> Get(string recordId)
		{
			var res = await api.Get(Id, recordId);
			return new LeaderboardRecord(res.Rank, res.Score, res.Rank != null);
		}
		public async Task<double> Increment(string recordId, double amount)
		{
			var res = await api.Increment(Id, recordId, amount);
			return res.Score;
		}
		public async Task<Dictionary<string, double>> GetRange(int from, int to)
		{
			var res = await api.Range(Id, from, to);
			return res.Result;
		}
		public async Task<int> Count()
		{
			var res = await api.Count(Id);
			return res.Count;
		}
		public async Task<int> Remove(string recordId)
		{
			var res = await api.Delete(Id, recordId);
			return res.Deleted;
		}
		public async Task<bool> Clear()
		{
			var res = await api.Clear(Id);
			return res.Cleared;
		}
	}

	public struct LeaderboardRecord
	{
		public int? Rank;
		public double? Score;
		public bool Exist;

		public LeaderboardRecord(int? rank, double? score, bool exist = true)
		{
			Rank = rank;
			Score = score;
			Exist = exist;
		}
	}
}
