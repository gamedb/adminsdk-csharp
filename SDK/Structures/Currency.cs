﻿using IndieBackendAdmin.API.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Structures
{
	public class Currency
	{
		public string Name { get; private set; }

		private readonly Currencies api;

		public Currency(string name, Currencies api)
		{
			Name = name;
			this.api = api;
		}

		public async Task<double> Set(string profileId, double amount)
		{
			var res = await api.Set(profileId, Name, amount);
			return res.Account[Name];
		}

		public async Task<double> Get(string profileId)
		{
			var res = await api.Get(profileId);
			return res.Account[Name];
		}

		public async Task<double> Increment(string profileId, double amount)
		{
			var res = await api.Increment(profileId, Name, amount);
			return res.Account[Name];
		}

	}

}
