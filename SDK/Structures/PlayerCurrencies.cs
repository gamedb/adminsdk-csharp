﻿using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Structures
{
	public class PlayerCurrencies
	{
		public string Id { get; private set; }

		private readonly API.Services.Currencies api;

		public PlayerCurrencies(string profileId, API.Services.Currencies api)
		{
			Id = profileId;
			this.api = api;
		}

		public async Task<double> Get(string currency)
		{
			var res = await api.Get(Id);
			return res.Account[currency];
		}

		public async Task<double> Set(string currency, double amount)
		{
			var res = await api.Set(Id, currency, amount);
			return res.Account[currency];
		}

		public async Task<double> Increment(string currency, double amount)
		{
			var res = await api.Increment(Id, currency, amount);
			return res.Account[currency];
		}
	
	}

}
