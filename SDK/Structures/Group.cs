﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Structures
{
	public class Group
	{

		public string Id { get; private set; }
		public string Leader { get; private set; }
		public bool IsDataPublic { get; private set; }
		public Dictionary<string, object> Data { get; private set; }
		public List<string> Members { get; private set; }
		public bool Deleted = false;

		private API.Services.Groups api;

		public Group(API.Services.GroupDataResult data, API.Services.Groups api)
		{
			this.api = api;
			UpdateData(data);
		}

		public async Task Refresh()
		{
			var data = await api.Get(Id);
			UpdateData(data);
		}
		public async Task<bool> Delete()
		{
			var res = await api.Delete(Id);
			if (res.Deleted)
			{
				Id = null;
				Leader = null;
				Data = null;
				Members = null;
				Deleted = true;
			}
			return res.Deleted;
		}
		public async Task<bool> Join(string playerId)
		{
			var res = await api.Join(Id, playerId);

			if (res.Joined)
				Members.Add(playerId);

			return res.Joined;
		}
		public async Task<bool> Leave(string playerId)
		{
			var res = await api.Leave(Id, playerId);

			if (res.Left)
				Members.Remove(playerId);

			return res.Left;
		}
		public async Task<bool> SetData(Dictionary<string, object> data)
		{
			var res = await api.SetData(Id, data);

			if (res.Updated)
				Data = data;

			return res.Updated;
		}
		public async Task<bool> SetLeader(string leaderId)
		{
			var res = await api.SetLeader(Id, leaderId);

			if (res.Updated)
				Leader = leaderId;

			return res.Updated;
		}

		private void UpdateData(API.Services.GroupDataResult data)
		{
			Id = data.Id;
			Leader = data.Leader;
			IsDataPublic = data.IsDataPublic;
			Data = data.Data;
			Members = data.Members;
		}

	}
}
