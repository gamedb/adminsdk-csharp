﻿using IndieBackendAdmin.SDK.Structures;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Services
{
	public class Currencies
	{

		private API.Services.Currencies api;

		public Currencies(API.Services.Currencies api)
		{
			this.api = api;
		}

		public Currency UseCurrency(string name)
		{
			return new Currency(name, api);
		}

		public PlayerCurrencies UsePlayer(string profileId)
		{
			return new PlayerCurrencies(profileId, api);
		}

		public async Task<Dictionary<string, double>> Get(string profileId)
		{
			var res = await api.Get(profileId);
			return res.Account;
		}

		public Task<Dictionary<string, double>> Set(string currency, string profileId, double amount)
		{
			return Set(profileId, currency, amount);
		}
		public async Task<Dictionary<string, double>> Set(string profileId, Dictionary<string, double> values)
		{
			var res = await api.Set(profileId, values);
			return res.Account;
		}

		public Task<Dictionary<string, double>> Increment(string profileId, string currency, double amount)
		{
			return Increment(profileId, currency, amount);
		}
		public async Task<Dictionary<string, double>> Increment(string profileId, Dictionary<string, double> values)
		{
			var res = await api.Increment(profileId, values);
			return res.Account;
		}

	}

}
