﻿using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Services
{
	public class Auth
	{
		private API.Services.Auth api;

		public Auth(API.Services.Auth api)
		{
			this.api = api;
		}

		public async Task<JwtClaims> VerifyToken(string jwt)
		{
			var res = await api.VerifyToken(jwt);
			return new JwtClaims(res.Claims);
		}

	}

	public class JwtClaims
	{
		
		public string Aud { get; private set; }
		public long Exp { get; private set; }
		public long Iat { get; private set; }
		public string Jti { get; private set; }
		public string Sub { get; private set; }
		public AuthTokenTypes Type { get; private set; }

		public JwtClaims(API.Structures.Auth.JwtClaims claims)
		{
			if (claims.Iss != "indiebackend")
				throw new System.ArgumentException("Invalid token issuer, token can't be trusted");

			Aud = claims.Aud;

			//Note for future needs: DateTime is based on 01/01/1900 instead of 01/01/1970
			Exp = claims.Exp * 1000;
			Iat = claims.Iat * 1000;
			
			Jti = claims.Jti;
			Sub = claims.Sub;
			Type = (AuthTokenTypes) System.Enum.Parse(typeof(AuthTokenTypes), claims.Type.ToUpper());
		}

	}

	public enum AuthTokenTypes
	{
		AUTH,
		PROFILE
	}

}
