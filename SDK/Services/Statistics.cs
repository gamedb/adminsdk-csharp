﻿using IndieBackendAdmin.SDK.Structures;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Services
{
	public class Statistics
	{

		private API.Services.Statistics api;

		public Statistics(API.Services.Statistics api)
		{
			this.api = api;
		}

		public Statistic Use(string name)
		{
			return new Statistic(name, api);
		}

		public async Task<Dictionary<string, double>> Set(string profileId, string statsName, double value)
		{
			var res = await api.SetPlayer(profileId, statsName, value);
			return res.Stats;
		}

		public async Task<Dictionary<string, double>> Increment(string profileId, string statsName, double amount)
		{
			var res = await api.IncrementPlayer(profileId, statsName, amount);
			return res.Stats;
		}

		public async Task<Dictionary<string, double>> Get(string profileId)
		{
			var res = await api.GetPlayer(profileId);
			return res.Stats;
		}

		public async Task<bool> Delete(string profileId, string[] stats)
		{
			var res = await api.DeletePlayer(profileId, stats);
			return res.Deleted;
		}
		public async Task<bool> Delete(string profileId, List<string> stats)
		{
			var res = await api.DeletePlayer(profileId, stats);
			return res.Deleted;
		}


	}
}
