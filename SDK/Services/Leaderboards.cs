﻿using IndieBackendAdmin.SDK.Structures;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Services
{
	public class Leaderboards
	{

		private API.Services.Leaderboards api;

		public Leaderboards(API.Services.Leaderboards api)
		{
			this.api = api;
		}

		public Leaderboard Use(string name)
		{
			return new Leaderboard(name, api);
		}

		public async Task<int> Set(string leaderboard, string recordId, double score)
		{
			var res = await api.Set(leaderboard, recordId, score);
			return res.Added;
		}
		public async Task<LeaderboardRecord> Get(string leaderboard, string recordId)
		{
			var res = await api.Get(leaderboard, recordId);
			return new LeaderboardRecord(res.Rank, res.Score, res.Rank != null);
		}
		public async Task<double> Increment(string leaderboard, string recordId, double amount)
		{
			var res = await api.Increment(leaderboard, recordId, amount);
			return res.Score;
		}
		public async Task<Dictionary<string, double>> GetRange(string leaderboard, int from, int to)
		{
			var res = await api.Range(leaderboard, from, to);
			return res.Result;
		}
		public async Task<int> Count(string leaderboard)
		{
			var res = await api.Count(leaderboard);
			return res.Count;
		}
		public async Task<int> Remove(string leaderboard, string recordId)
		{
			var res = await api.Delete(leaderboard, recordId);
			return res.Deleted;
		}
		public async Task<bool> Clear(string leaderboard)
		{
			var res = await api.Clear(leaderboard);
			return res.Cleared;
		}

	}
}
