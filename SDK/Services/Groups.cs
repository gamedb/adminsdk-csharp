﻿using IndieBackendAdmin.API.Services;
using IndieBackendAdmin.SDK.Structures;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IndieBackendAdmin.SDK.Services
{
	public class Groups
	{

		private API.Services.Groups api;

		public Groups(API.Services.Groups api)
		{
			this.api = api;
		}

		public GroupBuilder Builder()
		{
			return new GroupBuilder(api);
		}

		public async Task<GroupData> Get(string groupId)
		{
			var res = await api.Get(groupId);
			return new GroupData(res);
		}

		public async Task<bool> Delete(string groupId)
		{
			var res = await api.Delete(groupId);
			return res.Deleted;
		}
		public async Task<bool> Join(string groupId, string playerId)
		{
			var res = await api.Join(groupId, playerId);
			return res.Joined;
		}
		public async Task<bool> Leave(string groupId, string playerId)
		{
			var res = await api.Leave(groupId, playerId);
			return res.Left;
		}
		public async Task<bool> SetData(string groupId, Dictionary<string, object> data)
		{
			var res = await api.SetData(groupId, data);
			return res.Updated;
		}
		public async Task<bool> SetLeader(string groupId, string leaderId)
		{
			var res = await api.SetLeader(groupId, leaderId);
			return res.Updated;
		}

	}

	public class GroupData
	{
		public string Id { get; private set; }
		public bool IsDataPublic { get; private set; }
		public string Leader { get; private set; }
		public List<string> Members { get; private set; }
		public Dictionary<string, object> Data { get; private set; }

		public GroupData(GroupDataResult res)
		{
			Id = res.Id;
			IsDataPublic = res.IsDataPublic;
			Leader = res.Leader;
			Members = res.Members;
			Data = res.Data;
		}
	}

	public class GroupBuilder
	{

		private API.Services.Groups api;

		private string leader;
		private Dictionary<string, object> data;
		private bool isDataPublic;
		private string[] members;

		public GroupBuilder(API.Services.Groups api)
		{
			this.api = api;
		}

		public GroupBuilder Leader(string leader)
		{
			this.leader = leader;
			return this;
		}
		public GroupBuilder SetData(Dictionary<string, object> data)
		{
			this.data = data;
			return this;
		}
		public GroupBuilder IsDataPublic(bool isDataPublic)
		{
			this.isDataPublic = isDataPublic;
			return this;
		}
		public GroupBuilder WithMembers(string[] members)
		{
			this.members = members;
			return this;
		}

		public async Task<Group> Create()
		{
			var res = await api.Create(new CreateGroupRequest()
			{
				Data = data,
				IsDataPublic = isDataPublic,
				Leader = leader,
				Members = members
			});
			return new Group(res, api);
		}

		public async Task<Group> FromId(string id)
		{
			var res = await api.Get(id);
			return new Group(res, api);
		}
	}

}
