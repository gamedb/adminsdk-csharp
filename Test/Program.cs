﻿using System;
using System.Threading.Tasks;

namespace Test
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				AsyncOperations().Wait();
			} catch(ArgumentException e)
			{
				Console.WriteLine($"Caught ArgumentException: {e.Message}");
			}
		}

		private static async Task AsyncOperations()
		{
			//await APITesting.Main();
			await SDKTesting.Main();
		}

	}

}
